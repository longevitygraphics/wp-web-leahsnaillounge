// Window Scroll Handler

(function($) {

	if (!lastScrollTop	) {
			var lastScrollTop = 0;
	}
    $(window).on('scroll', function(){
    	var st = $(this).scrollTop();
    	if (st > lastScrollTop	) {
    		//Scroll Down
    		//console.log(st);
    		if (st >= 160) {
    		$("#site-header").addClass("scrunch");
    		}
    	} else {
    		//Scroll Up
    		//console.log(st);
    		$("#site-header").removeClass("scrunch");
    	}
    	lastScrollTop = st;


    	if (st <= 160) {
    		$("#site-header").removeClass("scrunch");
    	}
    });

}(jQuery));