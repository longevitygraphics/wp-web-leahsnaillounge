<?php

	global $lg_tinymce_custom;
	
	$lg_tinymce_custom = array(
	    'title' => 'Heading Embellishments',
	    'items' =>  array(
	    	array(
				'title' => 'Flower Line Break',
	            'selector' => 'h1, h2, h3, h4, h5, h6',
	            'classes' => 'decorative-line-break'
			),
			array(
				'title' => 'Heading Lines',
	            'selector' => 'h1, h2, h3, h4, h5, h6',
	            'classes' => 'heading-lines'
			)
	    )
	);

?>