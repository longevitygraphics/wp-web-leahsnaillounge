<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

	$left_group = get_sub_field('2_column_overlay_left_side');
	$left_content = $left_group['2_column_overlay_content'];

	$right_group = get_sub_field('2_column_overlay_right_side');
	$right_content = $right_group['2_column_overlay_img'];
	$right_overlay = $right_group['2_column_overlay_overlay_text'];

	$grid_width = get_sub_field('grid_responsive');
	$w_lg = (int)$grid_width['item_per_row_large'];
	$w_md = (int)$grid_width['item_per_row_medium'];
	$w_sm = (int)$grid_width['item_per_row_small'];
	$w_xs = (int)$grid_width['item_per_row_extra_small'];

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-<?php echo $w_xs ?> col-sm-<?php echo $w_sm ?> col-md-<?php echo $w_md ?> col-lg-<?php echo $w_lg ?> text-side order-lg-initial order-2">
			<?php echo $left_content ?>
		</div>
		<div class="col-<?php echo $w_xs ?> col-sm-<?php echo $w_sm ?> col-md-<?php echo $w_md ?> col-lg-<?php echo $w_lg ?> image-side order-lg-initial order-1">
			<img src="<?php echo $right_content['url']; ?>" alt="<?php echo $right_content['alt']; ?>">
			<div class="overlay-copy"><?php echo $right_overlay; ?></div>
		</div>
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
