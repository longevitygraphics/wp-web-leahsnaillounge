<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<?php do_action('wp_content_bottom'); ?>
	</div>
	
	<?php do_action('wp_body_end'); ?>
	<?php $lg_option_footer_site_legal = get_option('lg_option_footer_site_legal'); ?>

	<footer id="site-footer">
		
		<div>
			<div id="site-footer-main" class="clearfix py-3 container-fluid justify-content-center align-items-start flex-wrap">
				<div class="row">
					<div class="site-footer-alpha col-md-3 col-lg-2 text-center text-md-left d-none d-md-flex align-items-start justify-content-center"><?php dynamic_sidebar('footer-alpha'); ?></div>
					<div class="site-footer-bravo col-md-3 col-lg-2 text-center text-md-left d-none d-md-block"><?php dynamic_sidebar('footer-bravo'); ?></div>
					<div class="site-footer-charlie col-md-3 col-lg-2 text-center text-md-left"><?php dynamic_sidebar('footer-charlie'); ?></div>
					<div class="site-footer-delta col-md-3 text-center text-md-left d-none d-lg-block"><?php dynamic_sidebar('footer-delta'); ?></div>
					<div class="site-footer-echo col-md-3 text-center text-md-left"><?php dynamic_sidebar('footer-echo'); ?></div>
				</div>
			</div>

			<?php if(!$lg_option_footer_site_legal || $lg_option_footer_site_legal == 'enable'): ?>
				<div id="site-legal" class="py-2 px-3">
					<div class="d-flex justify-content-center justify-content-md-between align-items-center flex-wrap">
						<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
						<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
					</div>
				</div>
			<?php endif; ?>
		</div>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>

<?php do_action('document_end'); ?>
